import random
import json
import argparse
import base64
import os
import logging
import time

from paho.mqtt import client as mqtt_client
from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad

AES_MODE =  ["ECB", "CBC", "CTR", "CFB", "OFB"]

class Configs:
    def __init__(self, broker, port, topic, username, password, client_id):
        self.broker = broker
        self.port = port
        self.topic = topic
        self.username = username
        self.password = password
        self.client_id = client_id

def setConfig():
    #Read config from file
    with open('config.json') as f:
        config = json.load(f)

    logging.debug("Config uploaded from file:")
    logging.debug(json.dumps(config, indent=4))

    config = Configs(config["broker"], config["port"], config["topic"], config["username"], config["password"], f'python-mqtt-{random.randint(0, 1000)}')

    return config


def connect_mqtt(config) -> mqtt_client:
    #Connect to mqtt broker
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
            os._exit(rc)

    client = mqtt_client.Client(config.client_id)
    client.username_pw_set(config.username, config.password)
    client.on_connect = on_connect
    client.connect(config.broker, config.port)
    return client


def subscribe(client: mqtt_client, config):
    # Subscribe to mqtt topic
    def on_message(client, userdata, msg):
        plaintext = dencrypt(msg.payload)
        logging.debug(f"Received `{plaintext}` from `{msg.topic}` topic")

    client.subscribe(config.topic)
    client.on_message = on_message

def dencrypt(ciphertext):
    # Decrypt the cyphertext and return the plaintext
    #secretKey = base64.b64decode(encodedKey)

    b64 = json.loads(ciphertext)

    ts = b64['ts']
    messageLength = b64['messageLength']
    keySize = b64['keyLength']
    secretKey = base64.b64decode(b64['key'])
    index = b64['index']
    aesMode = b64['mode']


    if (aesMode == "ECB"):
        logging.debug("Using ECB mode")
        ct = base64.b64decode(b64['ciphertext'])
        cipher = AES.new(secretKey, AES.MODE_ECB)
        plaintext = unpad(cipher.decrypt(ct), AES.block_size)
    elif (aesMode == "CBC"):
        logging.debug("Using CBC mode")
        iv = base64.b64decode(b64['iv'])
        ct = base64.b64decode(b64['ciphertext'])
        cipher = AES.new(secretKey, AES.MODE_CBC, iv)
        plaintext = unpad(cipher.decrypt(ct), AES.block_size)
    elif (aesMode == "CTR"):
        logging.debug("Using CTR mode")
        nonce = base64.b64decode(b64['nonce'])
        ct = base64.b64decode(b64['ciphertext'])
        cipher = AES.new(secretKey, AES.MODE_CTR, nonce=nonce)
        plaintext = cipher.decrypt(ct)
    elif (aesMode == "CFB"):
        logging.debug("Using CFB mode")
        iv = base64.b64decode(b64['iv'])
        ct = base64.b64decode(b64['ciphertext'])
        cipher = AES.new(secretKey, AES.MODE_CFB, iv=iv)
        plaintext = cipher.decrypt(ct)
    elif (aesMode == "OFB"):
        logging.debug("Using OFB mode")
        iv = base64.b64decode(b64['iv'])
        ct = base64.b64decode(b64['ciphertext'])
        cipher = AES.new(secretKey, AES.MODE_OFB, iv=iv)
        plaintext = cipher.decrypt(ct)

    print ("Atual time: " + str(time.time()))
    totalTime = time.time() - ts
    
    print(totalTime*1000)

    logging.debug("----->" + str(plaintext))

    return plaintext

def run():

    parser = argparse.ArgumentParser(description='Subscriber MQTT AES')
    #parser.add_argument('key', type=str, help='Encrypt/Decrypt Key')
    #parser.add_argument('aesMode', type=str, help='AES mode')
    parser.add_argument("-v", "--verbose", help="Increase output verbosity", action="store_true")

    args = parser.parse_args()
    #encodedKey = args.key

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    #Check selected mode
    #if (aesMode not in AES_MODE):
    #    print("Pay attention to the AES mode selected! It must be one of the following values: ")
    #    print(*AES_MODE, sep = ", ")
    #    os._exit(1)

    #print("Key: " + args.key)

    config = setConfig()
    client = connect_mqtt(config)
    #subscribe(client, config, encodedKey, aesMode)
    subscribe(client, config)
    client.loop_forever()


if __name__ == '__main__':
    run()
