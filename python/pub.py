import random
import time
import json
import string
import os
import base64
import argparse
import logging

from paho.mqtt import client as mqtt_client
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad

VOWELS = "aeiou"
CONSONANTS = "".join(set(string.ascii_lowercase) - set(VOWELS))
KEY_LENGTH = [16, 24, 32]
AES_MODE =  ["ECB", "CBC", "CTR", "CFB", "OFB"]

class Configs:
    def __init__(self, broker, port, topic, username, password, client_id):
        self.broker = broker
        self.port = port
        self.topic = topic
        self.username = username
        self.password = password
        self.client_id = client_id

def setConfig():
    #Read config from file
    with open('config.json') as f:
        config = json.load(f)
    logging.debug('Config uploaded from file:')
    logging.debug(json.dumps(config, indent=4))
    config = Configs(config["broker"], config["port"], config["topic"], config["username"], config["password"], f'python-mqtt-{random.randint(0, 1000)}')
    return config

def generate_word(length):
    #Generate a random string with x length
    word = ""
    for i in range(length):
        if i % 2 == 0:
            word += random.choice(CONSONANTS)
        else:
            word += random.choice(VOWELS)
    
    # print ("-----> " + str(len(word.encode('utf-8'))))
    return word

def connect_mqtt(config):
    #Connect to mqtt broker
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
            os._exit(rc)

    client = mqtt_client.Client(config.client_id)
    client.username_pw_set(config.username, config.password)
    client.on_connect = on_connect
    client.connect(config.broker, config.port)
    return client

def disconnect_mqtt(client):
    #Gracefully disconnect from mqtt broker
    client.disconnect()

def publish(client, config, encodedSecretKey, messageLength, aesMode, timer, keyLength,):
    # Generate a random message, ask to encrypt it and send it to the topic
    index = 0
    while True:
        index = index + 1
        time.sleep(timer)
        msg = generate_word(messageLength)
        logging.debug("Message: " + msg)
        ciphertext = encrypt(msg, encodedSecretKey, aesMode, messageLength, keyLength, index)
        result = client.publish(config.topic, ciphertext)
        status = result[0]
        if status == 0:
            logging.debug(f"Send `{ciphertext}` to topic `{config.topic}`")
        else:
            print(f"Failed to send message to topic {config.topic}")

def encrypt(msg, encodedSecretKey, aesMode, messageLength, keyLength, index):
    # Recieve the paintext, encrypt it using the key and mode selectec and return the cyphertext
    ts = time.time()
    print(ts)
    secretKey = base64.b64decode(encodedSecretKey)
    if (aesMode == "ECB"):
        logging.debug("Using ECB mode")
        cipher = AES.new(secretKey, AES.MODE_ECB)
        ciphertext = cipher.encrypt(pad(msg.encode('utf-8'), AES.block_size))
        ct = base64.b64encode(ciphertext).decode('utf-8')
        result = json.dumps({'mode': aesMode, 'ciphertext':ct, 'ts':ts, 'key':encodedSecretKey.decode('utf-8'), 'messageLength':messageLength, 'keyLength':keyLength, 'index':index})
    elif (aesMode == "CBC"):
        logging.debug("Using CBC mode")
        cipher = AES.new(secretKey, AES.MODE_CBC)
        ciphertext = cipher.encrypt(pad(msg.encode('utf-8'), AES.block_size))
        iv = base64.b64encode(cipher.iv).decode('utf-8')
        ct = base64.b64encode(ciphertext).decode('utf-8')
        result = json.dumps({'mode': aesMode, 'iv':iv, 'ciphertext':ct, 'ts':ts, 'key':encodedSecretKey.decode('utf-8'), 'messageLength':messageLength, 'keyLength':keyLength, 'index':index})
    elif (aesMode == "CTR"):
        logging.debug("Using CTR mode")
        cipher = AES.new(secretKey, AES.MODE_CTR)
        ciphertext = cipher.encrypt(msg.encode('utf-8'))
        nonce = base64.b64encode(cipher.nonce).decode('utf-8')
        ct = base64.b64encode(ciphertext).decode('utf-8')
        result = json.dumps({'mode': aesMode, 'nonce':nonce, 'ciphertext':ct, 'ts':ts, 'key':encodedSecretKey.decode('utf-8'), 'messageLength':messageLength, 'keyLength':keyLength, 'index':index})
    elif (aesMode == "CFB"):
        logging.debug("Using CFB mode")
        cipher = AES.new(secretKey, AES.MODE_CFB)
        ciphertext = cipher.encrypt(msg.encode('utf-8'))
        iv = base64.b64encode(cipher.iv).decode('utf-8')
        ct = base64.b64encode(ciphertext).decode('utf-8')
        result = json.dumps({'mode': aesMode, 'iv':iv, 'ciphertext':ct, 'ts':ts, 'key':encodedSecretKey.decode('utf-8'), 'messageLength':messageLength, 'keyLength':keyLength, 'index':index})
    elif (aesMode == "OFB"):
        logging.debug("Using OFB mode")
        cipher = AES.new(secretKey, AES.MODE_OFB)
        ciphertext = cipher.encrypt(msg.encode('utf-8'))
        iv = base64.b64encode(cipher.iv).decode('utf-8')
        ct = base64.b64encode(ciphertext).decode('utf-8')
        result = json.dumps({'mode': aesMode, 'iv':iv, 'ciphertext':ct, 'ts':ts, 'key':encodedSecretKey.decode('utf-8'), 'messageLength':messageLength, 'keyLength':keyLength, 'index':index})

    logging.debug(result)
    return result


def generate_secret_key_for_AES_cipher(keyLength):
	# AES key length must be either 16, 24, or 32 bytes long
	secret_key = os.urandom(keyLength)
	encoded_secret_key = base64.b64encode(secret_key)
	print ("Encoded Secret Key: " + str(encoded_secret_key.decode('utf-8')))
	return encoded_secret_key


def run():
    #Parse and validate args 
    parser = argparse.ArgumentParser(description='Publisher MQTT AES')
    parser.add_argument('keySize', type=int, help='The size of the encription key')
    parser.add_argument('messageSize', type=int, help='The size of the plain text')
    parser.add_argument('aesMode', type=str, help='AES mode')
    parser.add_argument('-t', "--timer", type=int, help='The time to wait between sending messages', default=5)
    parser.add_argument("-v", "--verbose", help="Increase output verbosity", action="store_true")

    args = parser.parse_args()
    keyLength = args.keySize
    messageLength = args.messageSize
    aesMode = args.aesMode
    timer = args.timer

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    #Check key length
    if (keyLength not in KEY_LENGTH):
        print("Pay attention to the key length! It must be one of the following values: ")
        print(*KEY_LENGTH, sep = ", ")
        os._exit(1)

    #Check selected mode
    if (aesMode not in AES_MODE):
        print("Pay attention to the AES mode selected! It must be one of the following values: ")
        print(*AES_MODE, sep = ", ")
        os._exit(1)

    #Print configs
    logging.debug("Key Length: " + str(keyLength))
    logging.debug("Message Length: " + str(messageLength))
    logging.debug("AES Mode: " + aesMode)

    config = setConfig()
    encodedSecretKey = generate_secret_key_for_AES_cipher(keyLength)
    client = connect_mqtt(config)
    client.loop_start()
    publish(client, config, encodedSecretKey, messageLength, aesMode, timer, keyLength)


if __name__ == '__main__':
    run()