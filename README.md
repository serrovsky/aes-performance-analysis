# aes-performance-analysis

Daily activities have been increasingly supported by intelligent devices and applications. Smart devices are constantly communicating through the Internet of Things (IoT) networks, either by sending collected data and notifying the actions taken or by receiving instructions for actions to be taken. Most of this communication requires the confidentiality of data through the usage of encryption algorithms, being the Advanced Encryption Standard (AES) algorithm one of the most commonly used.  However, how do the operation modes of AES algorithm perform in a resource-constraint device? This paper aims to evaluate the impact on the time to encrypt and decrypt different sized messages in IoT devices when using each one of the five AES modes of operation and the three key sizes defined. The test scenario was implemented using two programming languages, running on a Raspberry Pi device. The results achieved infers that Python was quicker and had a more homogeneous result set than JavaScript implementation in most AES operation modes. These results help to understand the trade-off between IoT devices’ security needs and delays in communication caused by the selection of the AES algorithm operation mode.

The code present in this repository, was the implementation base to perform the study described in the intruction.

## Python

1 - Install pyhon version 3 and pip tool on your machine

2 - Install and run EMQX Edge for example in localhost, or use your broker changing the configs in config.json file (`https://www.emqx.io/downloads#edge`)

3 - Install modules

`pip install -r requirements.txt`

4 - Start subscriber

`python3 sub.py`

5 - Start publisher

`python3 pub.py <key_size> <message_size> <aes_mode> [-t <time_between_messages | default 5 sec ] [-v]`




## JavaScript


## Article

This study was done by Luis Serra and Pedro Gonçalves, in reply to the practical work of the Computer Networks Security subject, of the Master in Cybersecurity and Digital Forensics, taught in the Leiria Polythencic Institute and under the orientation of the teachers Luis Frazão and Mário Antunes.

After, this work was submitted to the CISTI Conference of 2021.
